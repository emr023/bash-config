
# The header function prints numbered column names of big tsv files
header() {
        cat "$1" | head -n 1 | tr "\t" "\n" | nl
}

# Puts all files in current folder into subdirs with $1 number of files
split_to_subdirs(){
        nd=0
        ls | xargs -L $1 echo | \
        while read cmd;
        do
                nd=$((nd+1))
                dir=$(printf "dir_%04d" $nd)
                mkdir $dir
                mv $cmd $dir
        done
}

# My convenience aliases
alias proc='ps aux | grep $USER'
alias path='echo -e ${PATH//:/\\n}'
alias ll='ls -alF'
alias ..='cd ..'
# Adds timestamp to each line in terminal
export PROMPT_COMMAND="echo -n \[\$(date +%H:%M:%S)\]\ "
# Set default git editor
export GIT_EDITOR=vim
