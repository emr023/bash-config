# bash-config 
Contains my favorite convenience functions, aliases and terminal decorations.  
You can use them too. If you like.  

Install on debian for non-login terminals by executing:  
``wget -O - https://gitlab.com/emr023/bash-config/-/raw/main/.bashrc?inline=false | cat >> $HOME/.bashrc``

Then resource or logout/login.  
Enjoy.
